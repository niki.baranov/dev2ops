# Docker Compose

Docker compose is a tool that allows you to automate creation and initialization of docker containers.

The prerequisites for it is that all the necessary containers are pre-built and are correctly linked to in the docker-compose.yml file.

Docker compose allows you to set pretty much all the necessary settings that you might otherwise pass using `docker run` command as well.

In this case, the docker compose file will launch all the containers on the same machine, but it is possible to use different hosts using Docker Swarm mode

What makes docker compose a rather "funky" tool, is that in most cases you can simply copy a functional docker-compose.yml file and run it on any machine with docker installed without a need to manually configure anything.

---

## Installing Docker Compose

Run this command to download the current stable release of Docker Compose:
>`sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`

Apply executable permissions to the binary:
>`sudo chmod +x /usr/local/bin/docker-compose`

## Docker Compose example for MongoDB

To create a MongoDB container using docker-compose, copy the code below into a docker-compose.yml file and add your mongodb image url or just name if its locally hosted

    version: '3.7'

    services:
      mongodb:
        image: path or url to the image
        container_name: mongodb
        volumes:
          - mongodb_data:/data/db
        networks:
          - dev_ops_network
        restart: always

    volumes:
        mongodb_data:
                name: mongodb_data

    networks:
          dev_ops_network:
                  name: dev_ops_network

Run docker compose up to build the containers mentioned in the docker-compose file (you need to run the command from within the same directory as the docker-compose file)
>`docker-compose up -d`

To see the full docker compose file for this project, check the [docker_compose_realworld_full.yml](docker_compose_realworld_full.yml) -file, note you will need to add your image urls if you intend to use this.

Feel free to also checke the full Wordpess docker compose file: [docker_compose_wordpress.yml](docker_compose_wordpress.yml)

## Sources

[Docker compose (JAMK 'Service-oriented applications' -course info)](http://ttow0130.pages.labranet.jamk.fi/03.-Containerisation/02.docker-compose/)

[Installing Docker Compose (Docker.com) ](https://docs.docker.com/compose/install/)

[Docker compose file (Docker.com) ](https://docs.docker.com/compose/compose-file/)

[Docker Swarm feature highlights (Docker.com) ](https://docs.docker.com/engine/swarm/#feature-highlights)