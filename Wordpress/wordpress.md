# Wordpress

# Installing Wordpress on Ubuntu 18.04

To setup Wordpress you will first need to setup Apache and PHP on your server, once this is done we can start installing Wordpress itself.

First, we need to get the Wordpress package, lets put it into tmp folder and extract
>```cd /tmp```

>```curl -O https://wordpress.org/latest.tar.gz```

>```tar xzvf latest.tar.gz```

Since we are using Wordpress installation from a tmp directory, we need to create and configure a .htaccess file.
>```touch /tmp/wordpress/.htaccess```

>```chmod 660 /tmp/wordpress/.htaccess```

Also lets copy a sample configuration file
>```cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php```

Lets also create a directory for updating worpdress
>```mkdir /tmp/wordpress/wp-content/upgrade```

Lastly lets copy the whole thing, since we are using sudo, you will need its password
>```sudo cp -a /tmp/wordpress/. /var/www/html```

To avoid permission issues you will need to assign sudo rights to the www directory of apache.
>```sudo chown -R sammy:www-data /var/www/html```

>```sudo find /var/www/html -type d -exec chmod g+s {} \;```

>```sudo chmod g+w /var/www/html/wp-content```

```sudo chmod -R g+w /var/www/html/wp-content/themes```

>```sudo chmod -R g+w /var/www/html/wp-content/plugins```

## Setting up Wordpress configuration file
Before we can start playing with Wordpress we should get some unique details for your setup, run the command below and save the provided info.
>```curl -s https://api.wordpress.org/secret-key/1.1/salt/```




define('AUTH_KEY',         'U!F&vYl~k{a_og>ql2b[/1*/hc.>-]3;/KrJTzt&3l4FolHnDAW_A`Oq8HK=q=y<');
define('SECURE_AUTH_KEY',  '4(`b~U]-0GUf]5Y/nqC?zp4JyJw%$}Z]X#Nh4r~U,J]!H85R^>?MeFBHUH]KUilq');
define('LOGGED_IN_KEY',    '}^Ok>*#%;`A700b/$FMP.4Csse1K*$}u{qJPz8Z+1}`)KOjA_]`xIPiS%J/A3+!Y');
define('NONCE_KEY',        'u1I?Pkkk_7jBom={hjUV^Q)ft7az^[|f1=<j5U[40V=yQF*+CPr2(,*{}<(;tJ7+');
define('AUTH_SALT',        'HY+:9(nQ2es+enpY=^wI,_4[o%4#5=FUD]Wh(r9.zezA90}pS.aqLOk7^)56PJ5$');
define('SECURE_AUTH_SALT', 'R$n-a}U4>1[`bGtffexg|_f]bL$l4 mgH&th!PQh/ =?rK:2YMs_:pA-jQ5fu7Z|');
define('LOGGED_IN_SALT',   '1i&S4]#F=P|c0t=}9Y8f  p8Y.dTEEG~pFnAp)>DV8/-Tkae]+;VJur^5Hh;<&&)');
define('NONCE_SALT',       'Um[zOVAc~l$Mpqmwp_nS06o7LKUdd|R_u5I8NFON)h wyV(<CvF-YPed|/|?(20(');


# Sources
[How to install Wordpress with LAMP on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lamp-on-ubuntu-16-04)

https://www.garron.me/en/bits/mysql-bind-all-address.html



wordpress
wp_user
123456