# PHP

#
## Installing PHP on Ubuntu 18.04 server
To install PHP on apache2 run

>```sudo apt install php libapache2-mod-php php-mysql```

You might also want to reconfigure your apache to serve PHP files as the main files on the server so go and check apache dir conf:
>```sudo nano /etc/apache2/mods-enabled/dir.conf```

Simply change the order of the list to feture index.php first then hit CTRL+X to save the file. Then just restart the apache

>```sudo systemctl restart apache2```