# Wordpress with external MySQL server

After initial NodeJS & React & MongoDB with Haproxy setup i also wanted to experiment with a similar Wordpress because its the system I've used quite a bit in the past and very possibly will have to setup later for my personal projects.

This part is divided into 4 simple steps, but you might also want to check out the [VirtualBox port forwarding]() section as you will need to do couple of forwards to access this setup from your host machine.

This is very different setup compared to the last one because MySQL will also require creation of a database and a user for it before it can be connected with Wordpress. Wordpress itself is quite simple to clone but you will need to go through several steps to get it properly configured. On top of all that, Wordpress requires a functioning server with PHP for it to function so you will need to setup a HTTP server and setup PHP.

Oder-wise I recommend setting up MySQL before you setup the second machine.
Apache + PHP + Wordpress would also need to be setup in that particular order.

1. [Installing MySQL](wordpress/mysql.md)
1. [Installing Apache](wordpress/apache.md)
1. [Installing PHP](wordpress/php.md)
1. [Installing Wordpress](wordpress/wordpress.md)
