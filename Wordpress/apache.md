# Apache

## Installing Apache on Ubuntu 18.04 server

>```sudo apt install apache2```

You will also need to allow apache server to be accessed through web thus allowing web traffic, this can be done by checking and adjusting the firewall settings
>```sudo ufw app list```

Check the "Apache Full" profikle
>```sudo ufw app info "Apache Full"```

You can see that it allows the 80 and 443 ports, thats what we need so go ahead and apply that profile
>```sudo ufw allow in "Apache Full"```

Once this is done, go and check that you can access your by accessing your servers ip.
Be sure to adjust your VirtualBox settings and port forwarding if necessary.

To restart Apache
>```sudo systemctl restart apache2```

To check Apache status
>```sudo systemctl status apache2```