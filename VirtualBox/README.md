# Virtual Box

This Dev2Ops project uses various amounts of virtual machines depending on the "section" of the Dev2Ops course. For each setup I will try to guide you on how to setup port forwarding that you will likely need when working with multiple instances of virtual machines.

In this case I used a MacBook Pro 15" with 1TB SSD and 16G of RAM and later made similar setup on a desktop system using Windows. Since I used Ubuntu Server 18.04 LTS as the OS for each of the machine, actual commands and installation of programs and tools remained the same.

Alternatively, you can use Ubuntu Server 18.04 LTS Mini version, but that would require installation of some of the tools that will likely be missing.

To use 64-bit version of VMs, you might need to enable virtualization in your computers BIOS settings as well.

---

## Basic specs

To start with, I used the following specs for all my VMs:

- 2GB RAM
- 1-2 CPU
- 30GB HDD

Obviously the selected setup is a serious overkill, considering what the machines would be used for, but it is nevertheless a good way to setup a default VM, if you have the resources to spare that is.

To use a 4 server setup using Virtual Box, you will need to make some extra configuration, primarily to create Port Forwarding so that you have SSH access to install packages as well as a direct HTTP access to test and verify that each server does whats intended.

In this section you will find instructions on how to set up a NAT Network that will be used for this project. I chose to use NAT Network instead Bridged network setup for my VMs because it gave me a better control of IPs used by virtual machines (since I worked with VMs on school premises as well as at home that made total sense). Working with NAT network made it necessary to setup a SSH port forwarding for each machine (obviously I could've simply used the VM directly but using SSH allowed me to copy-paste more easily :smile:)

---

## The 4 VM setup

The 4 VM setup might sound weird but it would make sense if you consider what those 4 machines could represent, perhaps its just a very high traffic site, or maybe there's a need to serve all types of content from a different location. In any case, setting this thing up using VMs will require port forwarding, and even more port forwarding if you prefer to use NAT Network setup (as I did).

Basically you will need to setup a port forwarding for each SSH port of each machine (if you want SSH connection ofc) as well as a separate HTTP forward for those VMs that will be hosting something that can be accessed via HTTP (ergo NodeJS, React and Haproxy stats). For setup of each of those systems, please check their specific section [RealWorld NodeJS](), [RealWorld React]() and [HAproxy]().

For setting up SSH for each service you need to get the machine up and running first, then simply check the VMs IP address, if you're working on a NAT Network setup, the address should be something like `10.0.2.X`.

Once you got that IP go on to VirtualBox Properties->Networking and create a new Network, then simply add one new rule fore each port forward you need. To the host cell input `127.0.0.1` to the FROM port input 3001 (I used 3000s for each SSH port, you can use pretty much any port that is not used on your system 3000 something is often a good choice). Then input the VM IP that you checked, from already up and running VM `10.0.2.15` and to the VM PORT section add 22, which is the default SSH port. The end result should look something like this:

`127.0.0.1:3001 10.0.2.15:22`

You will need to do the similar forwarding for each direct HTTP access that you might need (like the aforementioned NodeJS and React), this will allow you to test your setup more easily. For HTTP redirection you can also use pretty much any unset port, I would recommend avoiding using the default port 80 or 8080 for VM redirection, just in case you forget the machine is running...

The end result for your HTTP redirection would look something like this:

`127.0.0.1:3080 to 10.0.2.15:3080`

    Note, for the HTTP redirection to work you will need to configure that same port in the http broadcasting service like NodeJS or apache.

This is how my VirtualBox forwarding table ended up looking once i got all 4 VMs up and running:

![VirtualBox 4 VM port forwarding](virtualbox-4vm-port-forwarding.png)

---

## The single VM setup for Docker

Using multiple VMs was not necessary once we moved to the Docker section of the course. However, some port forwarding was still necessary because there was still need to access the VM by SSH. It was also necessary to get direct HTTP access to the services running on Docker containers. Essentially access to HTTP service running off a container required a double port forwarding setup; one forward from host to VM and another from within VM to docker container (`-p 3001:80`), basically traffic from `127.0.0.1:3001` was first redirected to a specific VM port and then VM redirected that traffic to specified container port. Sounds complicated i know. To setup docker port forwarding please check the [Docker section]().

This is how my port forwarding setup looked like for the docker setup. At first, I chose not to setup HAproxy machine at all, because it didn't really seem to have any point. Later though, specifically during the GCE Google Cloud part of the course, having a Haproxy docker container was convenient.

![VirtualBox port forwarding for 3 docker containers](virtualbox-port-forwarding-for-docker.png)

---

## Virtual Box and Docker compose

To my surprise (and yours too...) there was no need to do any extra configuration to run docker containers through docker compose, the same setup as for a single VM setup for Docker worked exactly the same way. For this to actually work like that, you will need to specify ports that will be used upon initiation of images by docker compose. For details on that, check the [Docker compose]() section.

## Sources

[VirtualBox.org Downloads](https://www.virtualbox.org/wiki/Downloads)

https://www.howtoforge.com/tutorial/ubuntu-lts-minimal-server/
