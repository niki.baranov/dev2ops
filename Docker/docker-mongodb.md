# MongoDB with Docker

Building a MongoDB container from scratch can require more knowledge of what MongoDB needs for it to successfully be installed (as in this case...) So it might be a better option to use a ready made, official MongoDB image and pull it from DockerHub.

---

## Building a MongoDB container

To create a mongodb container using an official MongoDB image use:
>`docker pull mongo`

This will create a mongodb image in your system, which can then be accessed by running
>`docker run --name mongodb --net dev_ops_network -p 27017:27017 -d mongo`

In this case mongodb container is also added to dev_ops_network docker network

---

## Building your own MongoDB image

If you prefer yo build your own MongoDB image...

To build a mongodb image called mongodb
>`docker build -t mongodb .`

    To create your own mongodb docker image you will also need to create a dockerfile with instructions regarding which packages to install.
    Check my examples of dockerfiles for NodeJS react and HAproxy

## Sources

[Create a MongoDB Docker container (Coderwall.com)](https://coderwall.com/p/odc3tw/create-a-mongodb-docker-container)

[Getting started mongodb docker container deployment (Thepolyglotdeveloper.com)](https://www.thepolyglotdeveloper.com/2019/01/getting-started-mongodb-docker-container-deployment/)
