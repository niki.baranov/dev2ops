# Docker

Docker operates using containers that contain a required filesystem and some default container settings. Docker images are significantly smaller than for example VirtualBox images, in addition you can run several containers on a single virtual machine.
For this part of the course I used a single VM running Ubuntu serer LTS (18.04) with 30Gb storage and 2Gb RAM.

Ready docker images can be found at [Dockerhub](https://hub.docker.com/)
When installing a docker image, by default if the system cant locate it locally, it will attempt to download it from Dockerhub.

    One of the most important things about Docker and its containers is that you should never save any information inside a container (like a database), this is due to their inherit use, they are created and destroyed constantly so saving any data inside them would result in loss of it. 

---

## Docker & RealWorld project

This docker project will re-create previously created 4 VM setup of DB, API and Frontend using docker containers. 

HAProxy VM will not be included in this part of the project.

In the next portion of the project Docker Compose will be used to automate creation and initiation of the RealWorld Mongo+NodeJS+React setup, check it here: [Docker Compose]().

    Keep in mind that for the containers to work, you will need to clone RealWorld project NodeJS and React git repositories and make small configurations to them within the VM.

    Alternatively, you can clone those projects to your git, make configurations there and then use that instead of original.

To check how I created containers for each of the VM check out the following:

- [Creating a MongoDB container](docker-mongodb.md)
- [Creating a NodeJS container](docker-nodejs.md)
- [Creating a React container](docker-react.md)
    - [Creating a Haproxy container (incomplete)](docker-haproxy.md)

For basic Docker commands and usage, check the guide below.

---

## Installing Docker

Before using Docker you will obviously need to set it up on your machine (or VM as in this case). I will use the package installers, but I'm quite sure there are other ways as well. Docker can also be setup for Windows and MacOS but I will not be doing so in this case as my VM is running Ubuntu linux.

Before any package installation, its a good practice to update Ubuntu packages (because just in case...)

You might want to check for possible older version of Docker first
>`sudo apt-get remove docker docker-engine docker.io containerd runc`

Update packages:
>`sudo apt-get update` or `sudo apt update`

Install some required packages
>`sudo apt-get -y install \apt-transport-https \ca-certificates \curl \gnupg-agent \software-properties-common`

Get docker GPG key
>`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

You can also double check that the fingerprint matches the correct key
>`sudo apt-key fingerprint 0EBFCD88`

You would get something like this as a result:

        pub   rsa4096 2017-02-22 [SCEA]
            9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
        uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
        sub   rsa4096 2017-02-22 [S]

This will setup the repository to use, in this case "stable", you can change the setting by passing "nightly" or "test" instead.
>`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

And this will install the latest version of Docker Engine Community
>`sudo apt-get update && sudo apt-get -y install docker-ce docker-ce-cli containerd.io`

---

## Using Docker

To make your life simpler, you might want to add your docker user to the sudo list, otherwise you will need to use sudo with every single docker command. Don't forget to logout / in or restart for the change to be in effect.

>`usermod -a -G docker your_docker_user_name` or simply `sudo usermod -a -G docker ${USER}`

These are the basic docker commands that you will likely end up using quite a bit.

Show docker process status
>`systemctl status docker.service`

Start docker service
>`systemctl start docker.service`

Restart docker service
>`systemctl restart docker.service`

Show docker process logs
>`journalctl -u docker.service`

Start a new container container from an image
>`docker run --name container_name -d image_name`

List all active docker container
>`docker ps`

Stop a container
>`docker stop container_name`

Start a container
>`docker start container_name`

Remove a container (container must be stopped first)
>`docker rm container_name`

Stop all docker containers
>`docker stop $(docker ps -a -q)`

Stop all docker containers (containers must be stopped first)
>`docker rm $(docker ps -a -q)`

---

## Managing Docker images

List all installed images
>`docker images`

Remove specific image
>`docker rmi image_name`

Download docker image from dockerhub, you can also use direct link
>`docker pull image_name`

---

## Mounting directories to containers

Example (don't use *tmp* as storage...):

>`mkdir /tmp/www`
>
>`docker run --name container_name -p vm_port:container_port -v vm_host_folder:container_folder_path:ro -d image_name`

`"-v /tmp/www:/usr/share/nginx/html:ro \ -d \ nginx" explained:`

    /tmp/www:               = source directory (vm host)
    /usr/share/nginx/html:  = destination directory (container)
    ro                      = optional parameter (read only)

---

## Running tools within docker containers

-it command is only necessary when trying to use interactive tools
>`docker exec -it name_of_the_container bash`

When using non-interactive tools just pass the command itself
>`docker exec name_of_the_container cat /etc/passwd`

---

## Copying to/from container

From container to host
>`docker cp container_name:path_of_file path_of_file_at_vm_host`

From host to container
>`docker cp path_of_file_at_vm_host container_name:path_of_file`

---

## Docker network controls

Create docker network
>`docker network create my_own_network`

Inspect a specific network
>`docker network inspect network_name_to_inspect`

Remove a network
>`docker network rm network_name_to_remove`

To assign a specific network to a specific container when using `docker run` add a `--net network-name` command

---

## Running a container and adding it to a specific network

>`docker run -dit --rm --name container --net my-network busybox`

    -dit                = runs the container interactively and in the background
    --rm                = will automatically remove a same named container
    --name kontti1      = name of the container
    --net mynetwork     = what network to use
    busybox             = name of image to base container on

---

## Building a container

Before you can run a container you will first need to make a docker image of it using specific settings and commands. These usually refer to what base filesystem/os to use, what environmental variables to set, what directories to use and other things that you want to automate so that you won't need to do them manually. For e.g. these can be updates or setup of specific packages that you know that you will use, like NodeJS or React.

Docker uses an instruction file called `dockerfile` to hold instructions regarding what needs to be included in the created images.

Detailed description of dockerfile can be either [Googled](https://www.google.com/search?q=dockerfile) or checked from dockers homepage: [Dockerfile referenced](https://docs.docker.com/engine/reference/builder/).

Use the following command within the same folder as your `dockerfile`:
>`docker build -t name_of_the_container` to build the container.

The `-t name_of_the_container`-command will instruct the docker build to assign a specific name to the image.

## Commands to run each container

The commands below would download the missing image in case they are not yet locally available.

Run mongodb container, redirect 27017 to 27017
>`sudo docker run --name mongodb -p 27017:27017 -d full_url_of_the_image:optional_tag`

Run nodeapi container, redirect 80 to 3000, use internal mongodb ip
>`sudo docker run --name nodeapi -p 80:3000 -e MONGODB_URI=mongodb:/internal_ip_of_mongodb_machine/conduit -d full_url_of_the_image:optional_tag`

Run react container, redirect 80 tp 4100 use public API IP
>`sudo docker run --name react -p 80:4100 -e REACT_APP_API_ROOT=http://public_ip_of_api_machine/api -d full_url_of_the_image:optional_tag`

Run haproxy container, redirect 8080 to 8080
>`sudo docker run --name haproxy -p 8080:8080 -d haproxy:v0`

## Sources

[How to install and use Docker on Ubuntu 18.04 (Digital Ocean)](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

[What is Docker (JAMK 'Service-oriented applications' -course info)](http://ttow0130.pages.labranet.jamk.fi/03.-Containerisation/01.docker/)

[Docker run options (Docker.com)](https://docs.docker.com/v17.12/engine/reference/commandline/run/#options)

[Docker Bind Mounts (Docker.com)](https://docs.docker.com/storage/bind-mounts/)
