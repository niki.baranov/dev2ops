# Haproxy with SSL

To use other than self signed SSL you would need to have a publicly available website and a static IP, otherwise SSL would simply get invalidated once the IP gets changed.

## Haproxy configuration for LetsEncrypt HTTPS

To certify your domain, simply run certbot with options below (change your domain of course), you should get a similar type of message if certification was successful.

>`sudo certbot certonly --standalone --preferred-challenges http --http-01-port 80 -d example.com -d www.example.com`

    IMPORTANT NOTES:
    - Congratulations! Your certificate and chain have been saved at:
    /etc/letsencrypt/live/your_domain.com/fullchain.pem
    
    Your key file has been saved at:
    /etc/letsencrypt/live/your_domain.com/privkey.pem
    
    Your cert will expire on 2020-01-08. To obtain a new or tweaked version of this certificate in the future, simply run certbot again. To non-interactively renew *all* of your certificates, run "certbot renew"
    
    - Your account credentials have been saved in your Certbot configuration directory at /etc/letsencrypt. You should make a secure backup of this folder now. This configuration directory will also contain certificates and private keys obtained by Certbot so making regular backups of this folder is ideal.
    
    - If you like Certbot, please consider supporting our work by:
    Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
    Donating to EFF:                    https://eff.org/donate-le

DOMAIN='your_domain.com' sudo -E bash -c 'cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/haproxy/certs/$DOMAIN.pem'

## Haproxy & LetsEncrypt on local machine

The following configuration is meant for the local installation of Haproxy using LetsEncrypt certificate fore SSL/443 connectivity.

For this setup to properly work, you will need to disable the frontend setup for port 80 and instead add bind to port 80 under the HTTPS configuration.

Since  NodeAPI and React frontend machines are running without SSL, you will also need to specify their ports to 80.

[Haproxy.conf with SSL on local machine](haproxy_conf_ssl_local.md)

    global
        maxconn 2048
        tune.ssl.default-dh-param 2048
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private
        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        # An alternative list with additional directives can be obtained from
        #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

    defaults
        option forwardfor
        option http-server-close
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

    #    frontend www-http
    #        bind *:80
    #        mode    tcp
    #        reqadd X-Forwarded-Proto:\ http
    #        acl api path_dir -i /api# matches api request
    #        use_backend srvr_backend if api
    #        default_backend srvr_frontend

    #        stats enable
    #        stats hide-version
    #        stats refresh 30s
    #        stats show-node
    #        stats auth admin:password
    #        stats uri  /haproxy?stats

    frontend www-https
        bind *:80
        bind *:443 ssl crt /etc/haproxy/certs/your_domain.com.pem
        reqadd X-Forwarded-Proto:\ https

        # Redirect if HTTPS is *not* used
        redirect scheme https code 301 if !{ ssl_fc }

        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /haproxy?stats

        acl letsencrypt-acl path_beg /.well-known/acme-challenge/
        acl api path_dir -i /api # matches api reques
        use_backend letsencrypt-backend if letsencrypt-acl
        use_backend srvr_backend if api
        default_backend srvr_frontend

    backend srvr_backend # backend API list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server node_api 10.132.0.46:80

    backend srvr_frontend # frontend list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server react_front 10.132.0.50:80

    backend letsencrypt-backend
        server letsencrypt 127.0.0.1:54321

## Sources

[Ubuntu bionic haproxy (Certbot)](https://certbot.eff.org/lets-encrypt/ubuntubionic-haproxy)

[How to secure haproxy with letsencrypt on ubuntu 14.04 (DigitalOcean)](https://www.digitalocean.com/community/tutorials/how-to-secure-haproxy-with-let-s-encrypt-on-ubuntu-14-04)

[Using SSL certificates with haproxy (Servers for hackers)](https://serversforhackers.com/c/using-ssl-certificates-with-haproxy)

[Using certbot (Certbot)](https://certbot.eff.org/docs/using.html#manual)

[How to https with hugo letsencrypt haproxy (Skarlso Github)](https://skarlso.github.io/2017/02/15/how-to-https-with-hugo-letsencrypt-haproxy/)

[Client certificatetion with haproxy (Loadbalancer.org)](http://www.loadbalancer.org/blog/client-certificate-authentication-with-haproxy/)

[Letsencrypt with haproxy(Servers for Hackers)](https://serversforhackers.com/c/letsencrypt-with-haproxy)