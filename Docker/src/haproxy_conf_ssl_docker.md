global
        maxconn 2048
        tune.ssl.default-dh-param 2048
        stats timeout 30s

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        # An alternative list with additional directives can be obtained from
        #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

    defaults
        option forwardfor
        option http-server-close
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000

        # haproxy error files, comment out if docker container does not have them
        #errorfile 400 /etc/haproxy/errors/400.http
        #errorfile 403 /etc/haproxy/errors/403.http
        #errorfile 408 /etc/haproxy/errors/408.http
        #errorfile 500 /etc/haproxy/errors/500.http
        #errorfile 502 /etc/haproxy/errors/502.http
        #errorfile 503 /etc/haproxy/errors/503.http
        #errorfile 504 /etc/haproxy/errors/504.http

    frontend www-https
        bind *:80
        bind *:443 ssl crt /etc/haproxy/certs/your_domain.com.pem
        reqadd X-Forwarded-Proto:\ https
        # Redirect if HTTPS is *not* used
        redirect scheme https code 301 if !{ ssl_fc }
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /haproxy?stats
        acl letsencrypt-acl path_beg /.well-known/acme-challenge/
        acl api path_dir -i /api # matches api reques
        use_backend letsencrypt-backend if letsencrypt-acl
        use_backend srvr_backend if api
        default_backend srvr_frontend

    backend srvr_backend # backend API list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server node_api 10.132.0.46:80

    backend srvr_frontend # frontend list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server react_front 10.132.0.50:80

    backend letsencrypt-backend
        server letsencrypt 127.0.0.1:54321