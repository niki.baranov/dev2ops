# React

For the front end of our test app we are using a [React based RealWorld app](https://github.com/gothinkster/react-redux-realworld-example-app).

Just as with the API part, we will use NodeJS base for it. The biggest difference between the two is the lack of necessary ENV variables. Instead we will need to modify the `src/agent.js` file.

## Building React container

Keep in mind that React is a front-end system so the API root configuration in the `src/agent.js` file in the react project directory, that you cloned, will first need to be changed to correspond to the API access point that your hosts front-end can reach, for e.g: `127.0.0.1:3315`

There are several ways that you can setup the API_ROOT variable within the src/agent.js file.

To make use of the docker network you want to assign a dynamic address to your API, normally this could be an URL or some specific IP address, in our case our API is running off the same machine, so the IP in this case would be 127.0.0.1 or localhost with a specific port that we have allowed to connect through, in this case 3315

If you assign a dynamic address, you then must include that variable in the React dockerfile, otherwise you would need to assign it when you run the container.

    Change API_ROOT to process.env.REACT_APP_API_ROOT
    For static IP: http://127.0.0.1:3315/api

MY dockerfile for RealWorld app React dockerfile looks like this:

    FROM node:10-alpine

    # Create app directory
    WORKDIR /usr/src/app

    # Install app dependencies
    # A wildcard is used to ensure both package.json AND package-lock.json are copied
    # where available (npm@5+)
    COPY package*.json ./

    RUN npm install
    # If you are building your code for production
    # RUN npm ci --only=production

    # Bundle app source
    COPY . .

    # default values
    ENV REACT_APP_API_ROOT=http://localhost:3315/api
    
    # expose to 4100 port
    EXPOSE 4100
    CMD [ "npm", "start" ]

Dont forget to place the `dockerfile` in the same directory as [React based RealWorld app](https://github.com/gothinkster/react-redux-realworld-example-app).  was cloned to.

Use the following command within the same folder as your `dockerfile`:
>`docker build -t name_of_the_container` to build the container.
---

## Running React container

To run react container usem as with above systems react is added to dev_ops_network
>`docker run --name name_of_the_container --net docker_network_to_use -p 4100:4100 -d name_of_the_image_to_use`
