# Haproxy setup for Google Cloud Engine

Setting up haproxy on Google GCE VM is just as straight forward as for the other parts of the setup.

## Haproxy on local machine (NON HTTPS)
The following configuration file is for basic Haproxy setup, without using Docker or SSL. This setup uses haproxy VM as an entry point, so direct access to API and frontend machines are not available.

Since in this case, a Google GCE VM is being uses, we can use internal IP addresses for both while using the public IP for Haproxy machine.

[Haproxy.conf to use on a local machine](haproxy_conf_local.md)

    global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin expose-fd listeners
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        # An alternative list with additional directives can be obtained from
        #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

    defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

    frontend http
        bind 0.0.0.0:3380
        mode http
        log             global
        maxconn 10
        clitimeout      100s
        srvtimeout      100s
        contimeout      100s
        timeout queue   100s
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /haproxy?stats
        acl api path_dir -i /api # matches api request
        # acl front path_dir -i / # all other requests
        use_backend srvr_backend if api
        default_backend srvr_frontend

    backend srvr_backend # backend API list
        balance roundrobin
        server node_api 10.132.0.46

    backend srvr_frontend # frontend list
        balance roundrobin
        server react_front 10.132.0.50

## Haproxy docker file

To build a Haproxy docker image you can simply use the official Haproxy image and add your own haproxy.cfg for configuration

    FROM haproxy:2.0.7-alpine
    COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg

Simply run this command to build your own image with correct configuration
>`docker build -t myproxy .`

## Uploading Haproxy image to GCE

First add a tag to your docker image
>`docker tag myproxy path_and_name_where_new_image_will_be_stored:optional_tag`

Now you can push it to gcloud, as long as your user has been authenticated.
>`docker push full_name_of_your_image:optional_tag`

## Haproxy with SSL via Docker container (HTTPS)

To run Haproxy docker container using a SSL certificate configured earlier, you will need to specify and mount a directory containing the certificate information. This also requires that the Haproxy configuration file is configured to use mounted certification. Simply add the mount command when you run the docker run.

>`--mount type=bind,source=path to the pem file to mount,target=path to where pem file to mount,readonly`

End result would look about like this:

>`docker run -it -p 443:443 -d --name myhaproxy --mount type=bind,source=/etc/haproxy/certs/example.com.pem,target=/etc/haproxy/cert/example.com.pem,readonly myproxy`

[Haproxy.conf SSL with Docker](haproxy_conf_ssl_docker.md)

added
    global
        maxconn 2048
        tune.ssl.default-dh-param 2048
        stats timeout 30s

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        # An alternative list with additional directives can be obtained from
        #  https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=haproxy
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

    defaults
        option forwardfor
        option http-server-close
        mode    http
        option  httplog
        option  dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000

        # haproxy error files, comment out if docker container does not have them
        #errorfile 400 /etc/haproxy/errors/400.http
        #errorfile 403 /etc/haproxy/errors/403.http
        #errorfile 408 /etc/haproxy/errors/408.http
        #errorfile 500 /etc/haproxy/errors/500.http
        #errorfile 502 /etc/haproxy/errors/502.http
        #errorfile 503 /etc/haproxy/errors/503.http
        #errorfile 504 /etc/haproxy/errors/504.http

    frontend www-https
        bind *:80
        bind *:443 ssl crt /etc/haproxy/certs/your_certificate_pem_file.pem
        reqadd X-Forwarded-Proto:\ https
        # Redirect if HTTPS is *not* used
        redirect scheme https code 301 if !{ ssl_fc }
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /haproxy?stats
        acl letsencrypt-acl path_beg /.well-known/acme-challenge/
        acl api path_dir -i /api # matches api reques
        use_backend letsencrypt-backend if letsencrypt-acl
        use_backend srvr_backend if api
        default_backend srvr_frontend

    backend srvr_backend # backend API list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server node_api 10.132.0.46:80

    backend srvr_frontend # frontend list
        # balance roundrobin
        redirect scheme https if !{ ssl_fc }
        server react_front 10.132.0.50:80

    backend letsencrypt-backend
        server letsencrypt 127.0.0.1:54321
