# NodeJS Docker image for RealWorld app

In the case of RealWorld app project that uses NodeJS to create an API system, we know that we will need NodeJS to be setup. Because we did this same setup earlier using separate VMs we also know that we have certain dependencies that NodeJS should have as well as some enviromental variables that are needed for our project to run. Armed with all that knowledge we can get the ball rolling.

---

## Building a NodeJS docker image for RealWorld app

MY dockerfile for RealWorld app NodeJS dockerfile looks like this:

    # Create a node 10 image using alpine version linux
    FROM node:10-alpine

    # Create app directory
    WORKDIR /usr/src/app

    # Install app dependencies
    # A wildcard is used to ensure both package.json AND package-lock.json are copied
    # where available (npm@5+)
    COPY package*.json ./

    RUN npm install
    # If you are building your code for production
    # RUN npm ci --only=production

    # Bundle app source
    COPY . .

    # default values, use IP for mongodb if not in the same docker network
    ENV NODE_ENV=production
    ENV SECRET=secret
    ENV MONGODB_URI=mongodb://mongodb/conduit

    # expose to 8080 port
    EXPOSE 8080
    CMD [ "node", "app.js" ]

Simply place the dockerfile into the same directory that you cloned the [RealWorld app NodeJS](https://github.com/gothinkster/node-express-realworld-example-app) example to.

Use the following command within the same folder as your `dockerfile`:
>`docker build -t name_of_the_container` to build the container.
---

## Running NodeJS container

To run the docker image we just made you will need to assign the correct ports `-p 3315:3000`, you might also need to redefine `MONGODB_URI` value to correspond your database address

To do this run the node container within the same docker network as mongodb container `--net docker_network_to_use` (you might need to create the network first)

If you are using a separate container for the database, the command should look something like below, in this case we are redirecting access from the host VM port 3315 to the containers port 3000. The `-d` means that the process will be executed in the background.

>`docker run -p 3315:3000 -d -e MONGODB_URI=mongodb://10.0.2.15/conduit name_of_the_image`

If you want to run NodeJS as well as mongodb in the same docker network, you should
also specify the `--net`. If you like, you can specify a name for the container to run as as wll by using `--name` command.

>`docker run --name name_of_the_container --net dev_ops_network -p 3315:3000 -d -e MONGODB_URI=mongodb://name_of_the_mongodb_container/conduit name_of_the_image`

In the above example, instead of using a direct IP of the database VM, a virtual network address provided by docker network will be used, instead of name_of_the_mongodb_container you use the name that mongodb container has.

Once mongodb and NodeApi are running you will be able to access the API through your host eg `127.0.0.1:3315` as long as you forward your host port 3315 to the virtual machines port 3315.

    Note that you dont need to pass the _MONGODB_URI_ variable, if the dockerfile is setup to use a virtual address provided by docker network

>`docker run --name name_for_container --net dev_ops_network -p 3315:3000 -e MONGODB_URI=mongodb://mongodb/conduit name_of_the_image_to_use`

Run command explained:

    --name name_for_container = assign a name for this container

    --net dev_ops_network = assign what docker network container should run in

    -p 3315:3000 = assign port forward from VM 3315 to container 3000

    -e MONGODB_URI=mongodb://mongodb/conduit = assign MONGODB_URI variable with value

    name_of_the_image_to_use = what image should be used to create this container
---

## Sources

[NodeJS docker webapp (NodeJS.org)](https://nodejs.org/de/docs/guides/nodejs-docker-webapp/)

[Containerizing NodeJS application with docker (Nodesource.com)](https://nodesource.com/blog/containerizing-node-js-applications-with-docker/)
