# Add add haproxy package to apt:
sudo add-apt-repository ppa:vbernat/haproxy-1.7

# Update apt packages (good idea bofore running install)
sudo apt -y update
sudo apt -y upgrade

# install haproxy
sudo apt -y install haproxy

# Copy haproxy.cfg
sudo cp /home/vagrant/haproxy.cfg /etc/haproxy/haproxy.cfg

# Start haproxy service
sudo systemctl restart haproxy
