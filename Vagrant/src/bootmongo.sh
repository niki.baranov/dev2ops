# install ubuntu mongodb package
sudo apt install -y mongodb

# rewrite access point from localhost to all IPs
sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mongodb.conf

# restart mongod service
sudo systemctl restart mongodb