# Node.js install for Ubuntu
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# install nodejs
sudo apt-get install -y nodejs

# clone nodejs project
git clone https://github.com/gothinkster/react-redux-realworld-example-app

#enter the node app folder and install npm dependencies
cd react-redux-realworld-example-app
sudo npm -y install

# Find the API_ROOT line in the src/agent.js and replace it, use ',' as delimiter
sudo sed -i "s,'https://conduit.productionready.io/api',process.env.REACT_APP_API_ROOT,g" /home/vagrant/react-redux-realworld-example-app/src/agent.js

# Copy myapp.service to systemd and run it
cp /home/vagrant/myreactapp.service /etc/systemd/system/myreactapp.service
sudo systemctl start myreactapp.service
