# Node.js install for Ubuntu
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# install nodejs
sudo apt-get install -y nodejs

# clone nodejs project
git clone https://github.com/gothinkster/node-express-realworld-example-app.git

#enter the node app folder and install npm dependencies
cd node-express-realworld-example-app
sudo npm -y install

# Copy mynodeapp.service to systemd and run it
cp /home/vagrant/mynodeapp.service /etc/systemd/system/mynodeapp.service
sudo systemctl start mynodeapp.service
