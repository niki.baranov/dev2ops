#
Vagrant.configure("2") do |config|

  # MongoDB vm
  config.vm.define "mongodb" do |mongodb|
    mongodb.vm.box = "ubuntu/bionic64"
    # hostname
    mongodb.vm.hostname = "mongo"
    # Networking
    mongodb.vm.network "private_network", ip: "192.168.66.11"

    # Settings for mongodb VM
    mongodb.vm.provider "virtualbox" do |mongodb_settings|
      # Set the name of the virtual box VM to test
      mongodb_settings.name = "mongodb"
      # Set RAM amount to 2048 MB
      mongodb_settings.memory = 2048
      # Set processor cores to 2
      mongodb_settings.cpus = 2
     end

    # Run shell script to install mongodb and configure it
    mongodb.vm.provision "shell", inline: "echo Hello mongo"
    mongodb.vm.provision :shell, path: "bootmongo.sh"
    mongodb.vm.synced_folder "db/", "/home/vagrant/db"
 
    mongodb.trigger.after :up do |trigger|
      trigger.name = "Hello world"
      trigger.info = "I am running after vagrant up!!"
      trigger.warn = "checking & importing DB from /home/vagrant/db/ if not empty"
      mongodb.vm.provision :shell, path: "restoremongodb.sh"

      # mongodb.vm.provision "file", source: "restoremongodb.sh", destination: "/home/vagrant/restoremongodb.sh"
    
      # this would run on host
      # trigger.run_remote = {inline: "mongodump --out=/home/vagrant/db/"}
      # trigger.run = {path: "restoremongodb.sh"}
    end
    mongodb.trigger.before :destroy do |trigger|
      trigger.name = "Bye world"
      trigger.info = "I am running before mongo is destroyed!!"
      trigger.warn = "Dumping database to /home/vagrant/db/"
      trigger.run_remote = {inline: "sudo mongodump --out=/home/vagrant/db/"}

      #mongodb.vm.provision "shell", inline: "sudo mongodump --out=/home/vagrant/db/"

      # config.vm.provision "shell", inline: <<-SHELL
      # echo "Provision the guest!"
      # SHELL

      # this would run on host
      # trigger.run_remote = {inline: "mongodump --out=/home/vagrant/db/"}
    end
   end

  # NodeJS vm
  config.vm.define "nodejs" do |nodejs|
    # what base to use
    nodejs.vm.box = "ubuntu/bionic64"
    # hostname
    nodejs.vm.hostname = "nodeapi"
    # Networking
    nodejs.vm.network "private_network", ip: "192.168.66.10"
    nodejs.vm.network "forwarded_port", guest: 3000, host: 3000

    # Settings for nodejs VM
    nodejs.vm.provider "virtualbox" do |nodejs_settings|
      # Set the name of the virtual box VM to test
      nodejs_settings.name = "nodejs"
      # Set RAM amount to 2048 MB
      nodejs_settings.memory = 2048
      # Set processor cores to 2
      nodejs_settings.cpus = 2
     end

    # Run shell script to install nodejs and configure it
    nodejs.vm.provision "shell", inline: "echo Hello Node"
    nodejs.vm.provision "file", source: "/Users/nikibaranov/Git/Vagrant/mynodeapp.service", destination: "/home/vagrant/mynodeapp.service"
    nodejs.vm.provision :shell, path: "bootnode.sh"
   end

  # React vm
  config.vm.define "react" do |react|
    # what base to use
    react.vm.box = "ubuntu/bionic64"
    # hostname
    react.vm.hostname = "react"
    # Networking
    react.vm.network "private_network", ip: "192.168.66.12"
    react.vm.network "forwarded_port", guest: 4100, host: 4100

    # Settings for react VM
    react.vm.provider "virtualbox" do |react_settings|
      # Set the name of the virtual box VM to test
      react_settings.name = "react"
      # Set RAM amount to 2048 MB
      react_settings.memory = 2048
      # Set processor cores to 2
      react_settings.cpus = 2
     end

    # Run shell script to install nodejs and configure it
    react.vm.provision "shell", inline: "echo Hello React"
    react.vm.provision "file", source: "/Users/nikibaranov/Git/Vagrant/myreactapp.service", destination: "/home/vagrant/myreactapp.service"
    react.vm.provision :shell, path: "bootreact.sh"
   end

  # Haproxy vm
  config.vm.define "haproxy" do |haproxy|
    # what base to use
    haproxy.vm.box = "ubuntu/bionic64"
    # hostname
    haproxy.vm.hostname = "haproxy"
    # Networking
    haproxy.vm.network "private_network", ip: "192.168.66.13"
    haproxy.vm.network "forwarded_port", guest: 4450, host: 4450
    haproxy.vm.network "forwarded_port", guest: 4460, host: 4460

    # Settings for haproxy VM
    haproxy.vm.provider "virtualbox" do |haproxy_settings|
      # Set the name of the virtual box VM to test
      haproxy_settings.name = "haproxy"
      # Set RAM amount to 2048 MB
      haproxy_settings.memory = 2048
      # Set processor cores to 2
      haproxy_settings.cpus = 2
     end

    # Run shell script to install nodejs and configure it
    haproxy.vm.provision "shell", inline: "echo Hello Haproxy"
    haproxy.vm.provision "file", source: "/Users/nikibaranov/Git/Vagrant/haproxy.cfg", destination: "/home/vagrant/haproxy.cfg"
    haproxy.vm.provision :shell, path: "boothaproxy.sh"
   end
# Update all
config.vm.provision :shell, path: "boot_all.sh"

end