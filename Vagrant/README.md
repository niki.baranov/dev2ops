# What is Vagrant

Vagrant is used for managing virtual machines using various VM providers (VirtualBox, HyperV etc.). By automating and unifying the setup process a lot of manual labor can be avoided and a setup of different environments can be sped up.

    Vagrant itself is not a virtualization service so it needs one of such to be installed on the system that will be running Vagrant.

---

## Configuration of Vagrant with Vagrantfile & bootstrap.sh

Because the idea behind Vagrant is to simplify the whole virtual machine and environment setup, all configurations will be handled by Vagrant instead of manually running a virtualization service and setting up a machine and then setting up OS etc.

All this is handled by a **Vagrantfile**, which is very much like a Dockerfile but for Vagrant. In this file you will tell Vagrant which base image to use for the VM, what resources to allocate for it, which ports to forward etc. Basically the same stuff you would normally do manually for the VM.

**Bootstrap.sh** is just a shell script that can be run once the VM is created. It would essentially be responsible for setting up the "extra" stuff on your VM. Things like running updates or installing some specific software like NodeJS.

To start, lets create a simple Ubuntu based **box**, simply copy the code below and create a Vagrantfile into directory you want to use for Vagrant. Each Vagrant "loop" starts with a "do" and ends with an "end", in the case below you have 2 loops, the main loop defines the box to use while the inner loop sets up the parameters of the created box.

    # -*- mode: ruby -*-
    # vi: set ft=ruby :

    Vagrant.configure("2") do |config|

    # Which OS to use and get from Vagrant cloud
    config.vm.box = "ubuntu/bionic64"

        # Use virtualbox as the provider and give couple of specifications
        config.vm.provider "virtualbox" do |v|
            # Set the name of the virtual box VM to test
            v.name = "test"
            # Set RAM amount to 2048 MB
            v.memory = 2048
            # Set processor cores to 2
            v.cpus = 2
        end

    # Port forwarding for the VM
    config.vm.network "forwarded_port", guest: 3000, host: 3000
    config.vm.network "forwarded_port", guest: 4100, host: 4100

    # Run a shell script once the VM is createdS
    config.vm.provision :shell, path: "bootstrap.sh"

    end

As mentioned afore, the bootstrap.sh script is used to install the other "stuff" to the created "box", in this example it could be as simple as updating ubuntu and installing NodeJS:

    # Ubuntu update
    sudo apt-get update
    sudo apt-get upgrade

    # Node.js installation on Ubuntu
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    sudo apt-get install -y nodejs

---

## Using Vagrant

The use of vagrant is pretty straightforward and very similar to Docker.

To create VM
>`vagrant up`

To setup a SSH to the created VM
>`vagrant ssh`

To remove aka destroy the VM
>`vagrant destroy`

---

## Creating a 4 VM setup

This time I will be using the same setup as previously using Node JS as the backend, React as front end, MongoDB as the database and HAproxy machine acting as load balancer. And just like before I'll be using RealWorld App projects NodeJS and React.

To get things going we must do the following:

- Create 4 virtual machines
- Install Ubuntu Server (18.04) on each machine
- Install and configure MongoDB
- Install and configure NodeJS on two machines
- Clone RealWorld projects NodeJS repository on one of NodeJS machines
- Clone RealWorld projects React repository on one of NodeJS machines
- Install and configure HAProxy on the 4th VM
- Configure port forwarding between Host and VMs

You can find the final Vagrantfile as well as additional .sh and .service files through the links below. The end result required having .service files to run NodeJS API as well as React front end in the background.

The .sh files are used to initialize shell commands needed to install required packages on all four VMs. In addition a haproxy.cfg is needed to run the proxy service.

You should also create a "db" folder in the same directory you would be running the Vagrantfile from, this directory will be used as the backup/restore location for the mongodb database, for this part to work, an inline shell script within vagrantfile is used to backup the db to the synced folder once vagrant destroy mongodb command is issued. Upon vagrant up command, vagrantfile also initializes a shell script to restore the background.

- [Vagrantfile (actual Vagrant file for the 4VM setup)](src/Vagrantfile)
- [boot_all.sh (general shell script for all)](src/boot_all.sh)
- [bootmongo.sh (shell script for MongoDB)](src/bootmongo.sh)
- [bootnode.sh (shell script for NodeJS)](src/bootnode.sh)
- [bootreact.sh (shell script for React)](src/bootreact.sh)
- [boothaproxy.sh (hell script for HAproxy)](src/boothaproxy.sh)
- [haproxy.cfg (haproxy configuration file)](src/haproxy.cfg)
- [mynodeapp.service (systemd file to run node app, containts necessary env variables)](src/mynodeapp.service)
- [myreactapp.service (systemd file to run react)](src/myreactapp.service)
- [restoremongodb.sh (shell script to restore the db from synced folder)](src/restoremongodb.sh)

---

### Vagrantfile

The vagrantfile for this project consist of loops to create each machine and then run specific scripts on them to achieve connectivity between them and correct configuration. Service files are also uploaded to the VMs via vagrantfile, these files need to be locally available on the VMs for them to work.

The sequence of actions is important, e.g. the DB machine needs to be up and running before we run the NodeAPI app, otherwise it could not connect to DB. The DB should also be restored so after installation of Mongodb the synced folder needs to be set.

The same works the other way around as well; Once the "kill" signal is sent, the database needs to be backed up BEFORE the machine is destroyed. In both cases Vagrant triggers are used.

Its also important to note that not all actions can be executed from vagrantfile script, there are rights/ownership cases where it is much more convenient to run scripts locally (as in case of service-files).

#### MongoDB section

Mongodb section is likely the most complex looking part of the vagrantfile as it utilizes triggers, sets sync folders, runs scripts and shell commands. In reality its pretty simple.

Sync folder is used to backup and restore the database, the actual database storage IS NOT CHANGED, so other configuration is not really necessary. Immediately after MongoDB VM is up and running  `bootmongo.sh` is executed as a shell script which installs Mongodb itself, once that part is done (`mongodb.trigger.after :up`) `restoremongo.sh` is executed to restore the db.

Once shutdown signal is sent but before Mongodb VM is shutdown (`mongodb.trigger.before :destroy`) a `restoremongodb.sh` is run to backup the db to the synced directory.

#### Node & React section

Node and React sections are extremely straightforward, IP and ports are set in the networking part, service files are copied to the VMS and shell scripts are run to install necessary programs.

#### Haproxy section

In Haproxy section you should remember to open up the port for haproxy as well to its stats "page" if you have that configured. Other than that, the haproxy.cfg is copied to home directory of vagrant and the `bootproxy.sh` is run to intsall haproxy and copy the cfg file.

---

## The extra .SH and .SERVICE files explained

### Boot_all

- Updates and upgrades the apt library

### Bootmongo

- Installs mongodb
- Uses **sed** to rewrite 127.0.0.1 to 0.0.0.0 to allow for non-IP restricted access to db server
- Restarts mongodb service.

The IP could be set to any IP that you would like to access the db, in this case I could use the IP of the NodeAPI VM as i set it within vagrant file so it is static and known.

### Bootnode

- Installs NodeJS
- clones the [https://github.com/gothinkster/node-express-realworld-example-app.git](https://github.com/gothinkster/node-express-realworld-example-app.git) to the VM
- Installs the nmp dependencies
- Copies mynodeapp.service from users directory to systemd directory
- Runs mynodeapp.service

### Bootreact

- Installs NodeJS
- clones the [https://github.com/gothinkster/react-redux-realworld-example-app](https://github.com/gothinkster/react-redux-realworld-example-app) to the VM
- Installs the nmp dependencies
- Uses **sed** to rewrite the API_ROOT setting within projects src/agent.js from <https://conduit.productionready.io/api> to process.env.REACT_APP_API_ROOT
- Copies myreactapp.service from users directory to systemd directory
- Runs myreactapp.service

The API_ROOT represents the frontend connection to the API so it needs to be accessable from the browser. process.env.REACT_APP_API_ROOT is used to enable dynamic setting of API root via variable.

### Boothaproxy

- Adds haproxy package to apt library
- Installs haproxy
- Copies the haproxy.cfg from local machine to the VM
- Restarts haproxy service.

### haproxy.cfg

- Configuration file for haproxy

### mynodeapp.service

- Sets environmental variables required to run nodeAPI
- Defines which js to run when called
- Runs nodejs in the background

Basically this service file created a runnable service that when called runs the realworld projects nodeAPI service with the provided variables. This way the service can also be run in the background and the Vagrantfile can be processed further.

### myreactorapp.service

- Sets environmental variables required to run React
- Defines the API access point
- Defines which js to run when called
- Runs npm in the background

### restoremongodb

- Checks if the synced (aka backup directory) is empty
- Restores the db from the /home/vagrant/db/ if not empty

The /home/vagrant/db/ is actually synced to the machine that runs Vagrantfile, the directory needs to be set in the Vagrantfile. The mongorestore command itself simply looks if there is a mongodb db in the provided directory and restores from there if so.

## Sources

[Vagrant downloads (Vagrantup.com)](https://www.vagrantup.com/downloads.html)

[Vagrant Triggers Configuration (Vagrantup.com)](https://www.vagrantup.com/docs/triggers/configuration.html)

[Vagrant – Copy Files and Folders From Host To Guest (Howtoprogram.xyz)](https://howtoprogram.xyz/2017/08/13/copy-files-folders-host-guest-vagrant/)

[Vagrantfile Explained: Setting Up and Provisioning with Shell (Sitepoint.com)](https://www.sitepoint.com/vagrantfile-explained-setting-provisioning-shell/)

[Understanding Systemd Units and Unit Files (DigitalOcean)](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files)

[Systemd: Service File Examples (Shellhacks.com)](https://www.shellhacks.com/systemd-service-file-example/)

[Bash Shell Check Whether a Directory is Empty or Not (Cyberciti.biz)](https://www.cyberciti.biz/faq/linux-unix-shell-check-if-directory-empty/)

[Run node.js service with systemd (Axllent.org)](https://www.axllent.org/docs/view/nodejs-service-with-systemd/)

[Configuring service unit files for use with systemd (Linuxnix.com)](https://www.linuxnix.com/configuring-service-unit-files-use-systemd/)

[https://www.devdungeon.com/content/creating-systemd-service-files (Devdungeon.com)](https://www.devdungeon.com/content/creating-systemd-service-files)

[How to use SED to find and replace URL strings with the “/” character in the targeted strings? (Stackoverflow.com)](https://stackoverflow.com/questions/16778667/how-to-use-sed-to-find-and-replace-url-strings-with-the-character-in-the-tar)