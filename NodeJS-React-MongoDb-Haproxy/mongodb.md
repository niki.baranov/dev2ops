# MongoDB

This setup uses MongoDB as per requirements of RealWorld Projects Node and React setups.
This guide is aimed for Ubuntu server 18.04 LTS system.

Mongo DB installation is extremely simple and you will require only minimal fiddling with the system.

I recommend setting this up first as you will need it later for Node.JS.

`Keep in mind` Ubuntu has its own version of mongodb which is **NOT** maintained by MongoDB. In this setup I will mainly guide you to use the default Ubuntu package, but I will also provide links for official MongoDB.

>This quide was compiled from multiple sources and trial&error, DigitalOcean tutorial is also extremely good: [How to Install MongoDB on Ubuntu 18.04 (Digitalocean.com)](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-18-04)

Before proceeding with installation. it is recommended to update and upgrade apt
>`sudo apt update`
>
>`sudo apt upgrade`

---

## Install MongoDB (Ubuntu version) on Ubuntu 18.04

Setting up default Ubuntu version of mongo is super easy:
>`sudo apt install -y mongodb`

check mongodb version
>`mongod --version`
sudo systemctl status mongodb

---

## Install MongoDB (Official) on Ubuntu 18.04

Import gpk key for it
>```sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4```

Add repository
>```echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list```

Check for updates:
>```sudo apt update```

Cupdate stuff:
>```sudo apt upgrade```

Install official mongodb:
>```sudo apt install mongodb-org```

After installation of MongoDB you should verify that it is running and that it is listening to the correct port:
>```sudo less /var/log/mongodb/mongod.log```
---

## Configuring Mongo DB

You might need to configure MongoDB to run on the correct IP
>```$ sudo vi /etc/mongod.conf```

Use netstat to check that mongodb is listening for the correct port and IP
>```netstat -ln```

In some situations you might need to delete mongo lock file
>```sudo rm /tmp/mongodb-27017.sock```
---

## Mongo DB service controls

To Enable service (startup automatically)
>```sudo systemctl enable mongod```

To Disable service (not to start automatically)
>```sudo systemctl disable mongodb```

Start service
>```sudo systemctl start mongod```

Stop service
>```sudo systemctl stop mongod```

Restart service
>```sudo systemctl restart mongod```

## Backing up mongodb

Backup db
>`mongodump --out=/data/backup/`

Restore db
>`mongorestore path_to_the_folder_with_backup/`
---

## Test MongoDB

You can test your MongoDV setup with a simple js code below that you can run on Node.JS machine.

    var MongoClient = require('mongodb').MongoClient;
    // Connect to the db
    MongoClient.connect("mongodb://{user}:{password}@{ip_address}:{27017}/{database}", function(err, db) {
    if(!err) {
        console.log("You are connected!");
      };
    db.close();
    });

## Sources

[Mongodb IP Binding (Mongodb.com)](https://docs.mongodb.com/manual/core/security-mongodb-configuration/)





# use this for the up to date mongodb version from developer
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
# echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list
# sudo apt -y update
# sudo apt -y install mongodb-org

# make mongodb listen to a certain ip (or all...)

#sudo sed -i 's,/var/lib/mongodb,/home/vagrant/db,g' /etc/mongodb.conf