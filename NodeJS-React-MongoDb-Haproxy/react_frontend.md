# Frontend VM with React
This setup uses RealWorld Projects React redux setup: https://github.com/gothinkster/react-redux-realworld-example-app

Before using this you will need to setup Node.js on your system, use the guide below.

## Installing React on Ubuntu 18.04

First nstall Node.js packed manager:
>```sudo apt install npm```





Clone a project
	git clone  https://github.com/gothinkster/react-redux-realworld-example-app.git

Setup dotenv https://github.com/motdotla/dotenv
	npm install dotenv

Set up cross env https://github.com/kentcdodds/cross-env
	npm install --save-dev cross-env


    npm install
    npm start
    vi src/agent.js
npm install --save-dev cross-env
npm install dotenv



React docker compose file


   react
     image: mysql:5.7
     depends_on:
       - db
     restart: always
     ports:
       - "8000:80"