# NodeJS + React + MongoDB + Haproxy on 4 VMs

During this section I created a 4 virtual machine system using Real World Project Node.js backend and React frontend with MongoDB and an additional machine running a haproxy service to act as a load balancer / proxy.

---

## The idea

The idea of the system is to have database, backend and frontend systems each on their own servers, in addition, for a more secure and robust setup a proxy server will act as the main access point.

All VMs are running on Ubuntu server 18.04 LTS through a VirtualBox Nat Network that is setup to allow direct access to each server via SSH as well as direct HTTP access points to API, frontend and loadbalancer for testing purposes.

Follow the instructions 1 through 5 to get the system up and running.

A good reference point what this setup is attempting to create can be found at: [5 Common server setups for your web application (DigitalOcean)](https://www.digitalocean.com/community/tutorials/5-common-server-setups-for-your-web-application)

1. [VirtualBox Network setup](virtual_box.md)
2. [MongoDB](/node_react_mongo_haproxymongodb.md)
3. [NodeJS + Express Backend & API](/node_react_mongo_haproxynode_express.md)
4. [React frontend with NodeJS](/node_react_mongo_haproxyreact_frontend.md)
5. [haproxy](/node_react_mongo_haproxyhaproxy.md)

## Real World app github links

[RealWorld example apps (Gothinkster @ Giithub)](https://github.com/gothinkster/realworld)

[RealWorld Example Node API(Gothinkster @ Giithub)](https://github.com/gothinkster/node-express-realworld-example-app)

[RealWorld React + Redux Frontend (Gothinkster @ Giithub)](https://github.com/gothinkster/react-redux-realworld-example-app)