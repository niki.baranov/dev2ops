# Node.JS + Express API
This setup uses Node.JS + Express backend from RealWorld Project: https://github.com/gothinkster/node-express-realworld-example-app

## Installing Node.JS + Express on Ubuntu 18.04
Node JS is the backend server component that will run our API connection to the MongoDB.
To setup node.js simply run the command below.
>```sudo apt install nodejs```

You should also install Node Package Manager aka npm
>```sudo apt install npm```

Clone the Real World Project Node.js repository
>```git clone https://github.com/gothinkster/node-express-realworld-example-app.git```

And finally install pacakges required by the project
>```npm install```

First some basics tools for Ubuntu
>```sudo apt-get install software-properties-common```

install onUbuntu:
>```curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -```

>```curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -```

LTS version:
>```curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -```

>```sudo apt-get install -y nodejs```

Clone the repo or create new project then run npm install
>```install NodeJS express npm install express```

Test node is installed  
>```curl -sL https://deb.nodesource.com/test | bash -```

Uninstall NodeJS from ubuntu
>```sudo apt-get remove nodejs```

>```sudo apt-get purge nodejs```

>```sudo apt-get autoremove```
---

## Test NodeJS installation
Lets use vim and create a sample app that we can use to test that NodeJS is actually working.
>```touch nodeapp.js```

>```vim nodeapp.js```

Now just add the code below

	var http = require('http');
	http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('Congratulations! node.JS has successfully been installed !\n');
	}).listen(3000);
	console.log('Server running at http://server-ip:3000/');

Start app:
>```node nodeapp.js```

Go to server http://server-ip:3000/ (change the server-ip part to your IP, eg. 127.0.0.1)

Starting the Node server
>```npm install -g nodemon```
---

# Sources
[NPM aka Packet manager (NodeJS.org)](https://nodejs.org/en/download/package-manager/)

[Node download (NodeJS.org)](https://nodejs.org/en/download/)

[Install & Uninstall NodeJS on Ubuntu (Journaldev.com)](https://www.journaldev.com/27373/install-uninstall-nodejs-ubuntu)