# haproxy (Proxy / Loadbalancer)
This setup is intended for usage with 1x frontend and 1x backend systems
To add additional servers, just add new servers to srvr_backend and srvr_frontend lists below.

This installation and setup is aimet for Ubuntu server 18.04 LTS

## Installing haproxy on Ubuntu 18.04

To add add haproxy package to apt:
>```sudo add-apt-repository ppa:vbernat/haproxy-1.7```

To update apt packages (good idea bofore running install)
>```sudo apt update```

>```sudo apt upgrade```

To install haproxy
>```sudo apt install haproxy```

To configure haproxy
>```nano /etc/haproxy/haproxy.cfg```

## Setup
To configure haproxy so that the system acts as a proxy/loadbalancer you need to assign it the IP to act as a "frontend" of the system, that is the "visible" IP that can be accessed.

Change the value of bind to the IP and port that you wish to use as the public access point.
>```bind 0.0.0.0:3389```

IPs setup for actual front / backends are mentioned in the
```srvr_backend``` and ```srvr_frontend``` lists

For the haproxy machine to act as a loadbalancer, simply setup additional servers and add their IPs in the corresponding lists of front and backend servers

The first frontend aka ```frontend stats``` configures the stats component of haproxy that allows recording of statistics regarding the usage of haproxy service.

The second frontend aka ```frontend http``` configures the public access point of haproxy machine as well ass intended actions upon which to redirect traffic.

```acl api path_dir -i /api```
    
    This part creates an access control list called "api" that concerns all traffic ending with /api (path_dir), we will use that to catch all calls made to API by frontend.

```default_backend srvr_frontend```
    
    This on the other hand, controls the default behaviour of our proxy server, in this case we will send all "non-api" traffic to the frontend machine.

More about ACL here: https://www.haproxy.com/blog/introduction-to-haproxy-acls/

Actual rules for this behaviour are configured on ```use_backend``` and ```default_backend``` lines. For api-requests we want to send traffic to our API server while for for non-api requests and as a default action, we will send traffic to our frontend server.

### The whole code in full:
```
frontend  stats
        bind  0.0.0.0:3389
        mode            http
        log             global
        maxconn 10
        clitimeout      100s
        srvtimeout      100s
        contimeout      100s
        timeout queue   100s
        stats enable
        stats hide-version
        stats refresh 30s
        stats show-node
        stats auth admin:password
        stats uri  /haproxy?stats

frontend http
        bind 0.0.0.0:3380
        mode http
        acl api path_dir -i /api # matches api request
        acl front path_dir -i / # all other requests
        use_backend srvr_backend if api
        default_backend srvr_frontend

backend srvr_backend # backend API list
   balance roundrobin
   server node_api 10.0.2.5:3000

backend srvr_frontend # frontend list
   balance roundrobin
   server react_front 10.0.2.6:3000
```

Before turning the service on, be sure to verify that your configuration is correct:
>```sudo haproxy -c -f /etc/haproxy/haproxy.cfg```

## Turning haproxy on
To turn haproxy service on use:
>```service haproxy start```

## Restarting haproxy
To restart haproxy use:
>```sudo service haproxy restart```


## Sources
[How to setup haproxy load balancing on ubuntu linuxmint (Tecadmin)](https://tecadmin.net/how-to-setup-haproxy-load-balancing-on-ubuntu-linuxmint/)

[Install haproxy ubuntu 18.04 LTS (Idroot)](https://idroot.us/install-haproxy-ubuntu-18-04-lts/)

[How to configure haproxy statics (Tecadmin)](https://tecadmin.net/how-to-configure-haproxy-statics/)

[Exploring haproxy stats page (Haproxy)](https://www.haproxy.com/blog/exploring-the-haproxy-stats-page/)

[Introduction to haproxy and load balancing concepts (Digital Ocean)](https://www.digitalocean.com/community/tutorials/an-introduction-to-haproxy-and-load-balancing-concepts#load-balancing-algorithms)

[Introduction to haproxy (Haproxy)](https://www.haproxy.com/blog/introduction-to-haproxy-acls/)

[ACLs (Haproxy)](https://www.haproxy.com/documentation/aloha/9-5/traffic-management/lb-layer7/acls/)

[How to using haproxy for load balancing (Linode)](https://www.linode.com/docs/uptime/loadbalancing/how-to-use-haproxy-for-load-balancing/)