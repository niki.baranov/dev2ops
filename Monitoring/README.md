# Monitoring

This section will explain the use of Grafana, Influxdb and Telegraf to provide a visual monitoring service for the 4 VM project.

Whether the system is created via with local virtual machines or using cloud services, the setup is pretty much the same.

In this case, instead of running the monitoring service from the already used VM, a new one was created to be used only for monitoring other services.

## What is Grafana

## What is Influxdb

## What is Telegraf

## Setting up a testbench

Before we dive into the full functional setup, lets test how the Grafana works by setting up a simpletest bench.

Clone the test package from github and run docker-compose:

>`git clone https://github.com/bcremer/docker-telegraf-influx-grafana-stack`
>
>`cd docker-telegraf-influx-grafana-stack`
>
>`docker-compose up`

Next, lets setup a basic metric generator

### Install PHP

>`apt install php7.2-cli`

### Install Composer

>`php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"`
>
>`php composer-setup.php`
>
>`php -r "unlink('composer-setup.php');"`
>
>`mv composer.phar /usr/local/bin/composer`



## Monitoring setup for 4 VM

To get the monitoring service up and running the following tasks will need to be completed:

1. Install Grafana on monitoring VM
1. Install Influx on monitoring VM
1. Install & Setup Telegraf service on mongo VM
1. Install & Setup Telegraf service on nodeapi VM
1. Install & Setup Telegraf service on react VM
1. Install & Setup Telegraf service on Haproxy VM

Telegraf service will send collected data to Influxdb on our monitoring VM which will then be translated into visual data using Grafana. Setting the system up is very straightforward but you might need to "Google" for ID numbers of various plugins that are supported by Grafana as there are many pre-made dashboards configurations you can simply import.

### Grafana installation



### Influx installation

>`echo "deb https://repos.influxdata.com/ubuntu bionic stable" | sudo tee /etc/apt/sources.list.d/influxdb.list`
>
>`sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -`
>
>`sudo apt-get update`
>
>`sudo apt-get install -y influxdb`
>
>`sudo systemctl enable --now influxdb`
>
>`sudo systemctl is-enabled influxdb`
>
>`systemctl status influxdb`

To set firewall rules just use this:

>`sudo apt-get install ufw`
>
>`sudo ufw enable`
>
>`sudo ufw allow 8086/tcp`

Dont send data to indluxdata
--reporting-disabled

    Keep in mind that if you are using a cloud service with networking, you might need to create firewall rules to allow ports used by Influx.

### Installation of Telegraf on service VMs (Mongo, NodeAPI, React, Haproxy)

>`wget https://dl.influxdata.com/telegraf/releases/telegraf_1.12.3-1_amd64.deb`
>
>`sudo dpkg -i telegraf_1.12.3-1_amd64.deb`
>
>`cat <<EOF | sudo tee /etc/apt/sources.list.d/influxdata.list deb https://repos.influxdata.com/ubuntu bionic stable
EOF`

`sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -`

`sudo apt-get update`

`sudo apt-get -y install telegraf`

`sudo systemctl enable --now telegraf`

`sudo systemctl is-enabled telegraf`

`sudo apt-get -y install vim`

`sudo vi /etc/telegraf/telegraf.conf`

### Configure Telegraf to send data to Influxdb VM

For Grafana to be able to show any data, there first needs to be some data to show. Once Telegraf service is installed you will need to do some small configurations to the telegraf.conf file.

Simply find the following lines from within `/etc/telegraf/telegraf.conf` -file and input correct values for your setup.

- add server = "tag name" to telegraf.conf
- add urls = ["http://35.187.12.153:8086"] (influxdb ip and port) to telegraf.conf
- uncomment db
- uncomment  user and pwd
- uncomment user agent?

Once changes are done, save the conf file and restart telegraf service
> `sudo systemctl restart telegraf`

Als check that service is actually running
>`sudo systemctl status telegraf`

Check that telegraf is sending data
>`telegraf --test`



grafana

## More about the subject

[Google Site Reliability Engineering books (Google.com)](https://landing.google.com/sre/books/)

## Sources

[Monitoring (JAMK 'Service-oriented applications' -course info)](http://ttow0130.pages.labranet.jamk.fi/14.-Monitoring/01.index/)

[Monitoring & observability tools (JAMK 'Service-oriented applications' -course info)](http://ttow0130.pages.labranet.jamk.fi/14.-Monitoring/02.monitoring-and-observability/)

[Google Site Reliability Engineering (Google.com)](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/)

[How to Monitor Service: The Fundamental Framework (Hakernoon.com)](https://hackernoon.com/service-monitoring-the-conceptual-framework-2ebffebb362d)

[How to Monitoring the SRE Golden Signals (Slideshare.net)](https://www.slideshare.net/OpsStack/how-to-monitoring-the-sre-golden-signals-ebook/)

[Install influxsb on Ubuntu 18.04 and Debian 9 (Computingforgeeks.com)](https://computingforgeeks.com/install-influxdb-on-ubuntu-18-04-and-debian-9/)

[How to Install and Configure Telegraf on Ubuntu 18.04 / Debian 9 (Computingforgeeks.com)](https://computingforgeeks.com/how-to-install-and-configure-telegraf-on-ubuntu-18-04-debian-9/)

[Installing on Debian / Ubuntu (Grafana.com)](https://grafana.com/docs/installation/debian/)

