# Google Cloud Platform

Google Compute Engine can be used for creation and management of Virtual Machines. It allows you to configure all the basic things of a virtual machine, much like its done on VirtualBox. You can also pick an OS image to use, so you end up having a ready to use VM with preinstalled version of OS you selected. In addition, you can host your Docker containers and setup Virtual Private Cloud (VPC) networks.

For this project I used the smallest VM Google could offer (at that time f1-micro with 1CPU and 614MB of RAM) with an Ubuntu 18.04 LTS Minimal OS.

## Rebuilding the 4VM setup with GCE using Docker

This time the 4VM RealWorld app project will be remade using GCE to create virtual machines, setup docker on all of them and utilize previously created docker images that were uploaded to GCE.

All other machines and images were pretty simple to setup but Haproxy required significant changes, specially after adding SSL.

To start with setting up our 4 VM setup we will need to do the following:

1. Create a VM using GCE for each part (4VM in total)

## Setting up GCE on NON-GCE VMs

Before you start using GCE you need to set it up on the VM you mean to use it on. This requires installing Google Cloud SDK as well as few other tools to enable authentication services.

    GCE SDK is preinstalled on GCE VMs

Google Cloud SDK is not a part of the default apt tool, so first you will need to add it.
>`echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list`

You will also need to sure apt-transport is installed.
>`sudo apt-get -y install apt-transport-https ca-certificates`

Next, import Google Cloud public key
>`curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -`

Now, we can proceed to install Google cloud SDK
>`sudo apt-get -y update && sudo apt-get -y install google-cloud-sdk`

Before you can initialize Google cloud services you will need to authenticate your user via browser using the link that the tool will provide.
>`gcloud auth login`

Start Google Cloud
>`gcloud init`

Now you can use pull and push commands to get docker containers from google.

Configure docker to use gcloud as auth gcloud auth configure-docker

---

## Using Docker with GCE

Unlike Google SDK, Docker is not preinstalled on GCE VMs, so you'll need to install it before you get to run or pull any image using it. You can refer to the [Docker]() section for installation guide.

If you try using docker to run/pull a container from GCE you will likely get the following error:

    Unable to find image 'path/name_of_the_image' locally
    docker: Error response from daemon: unauthorized: You don't have the needed permissions to perform this operation, and you may have invalid credentials. To authenticate your request, follow the steps in: https://cloud.google.com/container-registry/docs/advanced-authentication.
    See 'docker run --help'.

To put it simply, you need to authorize your user with Google

To be able to push and pull docker containers from Gcloud you will need to install Google Cloud SDK first. You will also need to authenticate your user either via gcloud auth OR by authenticating using 'alternative' methods.

>`gcloud auth print-access-token | docker login -u oauth2accesstoken --password-stdin eu.gcr.io`

You might need to fix the PATH variable used by the command:

    export PATH="${PATH}:/snap/google-cloud-sdk/current/bin"

Once you're authenticated you can use default docker commands to pull / push  docker images directly from / to the account.

Alternatively you can also simply authenticate docker to use gcloud with:

>`gcloud auth configure-docker`

Before pushing you will need to create specific tags to your images.
>`docker tag name_of_existing_image:optional_tag url_and_path_of_where_to_save/new_name_of_image:optional_tag`

e.g

>`docker tag oma-nginx:v0.1 eu.gcr.io/dev2ops/oma-nginx:v0.1`

## Sources

[Google Cloud SDK documentation (Google)](https://cloud.google.com/sdk/docs/)

[Google Docs Pushing and pulling images (Google)](https://cloud.google.com/container-registry/docs/pushing-and-pulling)

[Docker (Google)](https://cloud.google.com/sdk/gcloud/reference/docker)

[Deploying containers (Google)](https://cloud.google.com/compute/docs/containers/deploying-containers)

[Advanced authentication (Google)](https://cloud.google.com/container-registry/docs/advanced-authentication)
