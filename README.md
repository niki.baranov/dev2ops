# Dev2Ops (course diary / documentation)

This project is a course diary / documentation of sorts for the Dev2Ops course that was held at JAMK (University of Applied Sciences in Jyväskylä) between September and Novemvber of 2019.

The aim of this course diary is to compile what I've learned in an easy to use guide / documentation, that can be used by myself or others when starting up with various Dev Ops things like VirtualBox, Docker, Vagrant and GCE. Keep in mind that I WILL NOT go too deep into the tools or the used commands, for that you should check the documentation and tutorials for each particular tool.

## What is all this about

The dev2ops course started with a each participant choosing the type of project they want to work on/with during the course. In my case, I chose to setup a RealWorld app using NodeJS as the backend and React as a frontend. Alternative way would've been to create my own application to setup using the tools and technologies we would be introduced to, this would've essentially moved the focus from DevOps to actual Development. In addition, I also wanted to run the database as a separate entity and later added in a load balancer, so a total of four different systems would be made.

You will find each section in its own directory so feel free to browse about. I would recommend that you follow the same order that the course itself had, this is simply because that way you would see the difference in the workflow and more importantly the difference in the time used when doing things the "old-way" or the "new-way".

    At this moment this is still "work in progress..."

1. [Git](Git/)
1. [VirtualBox (mainly forwarding...)](VirtualBox/)
1. [Real World app with a twist (nodejs, react, mongodb, haproxy)](NodeJS-React-MongoDb-Haproxy/)
1. [Docker](Docker/)
1. [Docker compose](Docker-compose/)
1. [Vagrant](Vagrant)
1. [Google compute engine](Gce/)
1. [Monitoring](Monitoring/)
1. [Wordpress](wordpress/)

> More coming soon...

## Some useful linux tools / commands

Journalctl is a great tool to see whats been happening on the server. To see all kernel related logs use `-k`, to show the last 50 logs use `-n 50`, to show only the logs from the last boot use `-b`.
>`journalctl -kb -n 20`

If you are using a minimal OS version, you might need to setup an editing tool like VIm editor
>`sudo apt-get install vim`

## Sources

[How to use journalctl to view and manipulate systemd logs (DigitalOcean)](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs)

## Real World app github links

[RealWorld example apps (Gothinkster @ Giithub)](https://github.com/gothinkster/realworld)

[RealWorld Example Node API(Gothinkster @ Giithub)](https://github.com/gothinkster/node-express-realworld-example-app)

[RealWorld React + Redux Frontend (Gothinkster @ Giithub)](https://github.com/gothinkster/react-redux-realworld-example-app)
